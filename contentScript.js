class Folder {
  constructor(title) {
    this.title = title;
    this.children = [];
  }
}

class Bookmark {
  constructor(title, url) {
    this.title = title;
    this.url = url
  }
}

// Get current time and format
function getTime() {
  let date = new Date(),
    min = date.getMinutes(),
    sec = date.getSeconds(),
    hour = date.getHours();

  return "" +
    (hour < 10 ? ("0" + hour) : hour) + ":" +
    (min < 10 ? ("0" + min) : min) + ":" +
    (sec < 10 ? ("0" + sec) : sec);
}

class Storage {
  backgroundColor;

  getBackgroundColor() {
    return this.backgroundColor;
  }

  setBackgroundColor(backgroundColor) {
    this.backgroundColor = backgroundColor;
  }
}

let storage = new Storage();

window.onload = () => {
  fetchStorage()
    .then((storage) => {
      loadBackground(storage);
      clock();
      GetWeather();
      GetCatFact();
      getBookmarks();
    });
};
document.onkeypress = (e) => {
  search(e);
  search_visibility(e);
};


loadBackground = (storage) => {
  console.log(storage.getBackgroundColor());
  document.body.style.backgroundColor = storage.getBackgroundColor();
};


fetchStorage = () => {
  return new Promise(((resolve, reject) => {
    chrome.storage.sync.get("com.bev.startPage.backgroundColor", function (color) {
      let rec = color["com.bev.startPage.backgroundColor"];
      console.log("gotColor", rec);
      storage.setBackgroundColor(rec);
      resolve(storage);
    });
  }));
};

function GetCatFact() {

  fetch("https://catfact.ninja/fact?max_length=140")
    .then((response) => {
      response.json().then((resp) => {
        console.log(resp);
        document.getElementById("cat-fact").innerHTML = resp.fact;
      });
    });
}

function GetWeather() {
  let xhr = new XMLHttpRequest();
  // Request to open weather map
  let cityIdGlarus = "6458747";
  let apiKey = "bbe4ca14b9830f12ec3fd87a92ee15eb";

  xhr.open('GET', 'http://api.openweathermap.org/data/2.5/weather?id=' + cityIdGlarus + '&units=metric&appid=' + apiKey);
  xhr.onload = () => {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        let json = JSON.parse(xhr.responseText);
        document.getElementById("temp").innerHTML = json.main.temp.toFixed(0) + "°C";
        document.getElementById("weather-description").innerHTML = json.weather[0].description;
        document.getElementById("weather-icon").src = "http://openweathermap.org/img/w/" + json.weather[0].icon + ".png";
      } else {
        console.log('error msg: ' + xhr.status);
      }
    }
  };
  xhr.send();
}

function getBookmarks() {
  chrome.bookmarks.getSubTree("1", function (response) {
    let bookmarkMainFolder = response[0];
    let bookmarkFolder = [];
    for (let i = 0; i < bookmarkMainFolder.children.length; i++) {
      let bookmarkObject = createBookmarkObject(bookmarkMainFolder.children[i]);
      if (bookmarkObject instanceof Folder) {
        bookmarkFolder.push(bookmarkObject);
      }
    }
    console.log(bookmarkFolder);
    let bookmarkContainer = document.getElementById("bookmarkContainer");


    let bookmarkContainerInnerHtml = "";
    for (let i = 0; i < bookmarkFolder.length; i++) {
      bookmarkContainerInnerHtml += createFolderElement(bookmarkFolder[i]) + "\n";
    }
    bookmarkContainer.innerHTML = bookmarkContainerInnerHtml;
  });


//TODO FIXME
  function createBookmarkObject(bookmarkObject) {
    if (bookmarkObject.children) {
      return createFolderObject(bookmarkObject);
    } else {
      return new Bookmark(bookmarkObject.title, bookmarkObject.url);
    }
  }

  function createFolderObject(folderObject) {
    let folder = new Folder(folderObject.title);
    for (let i = 0; i < folderObject.children.length; i++) {
      if (folderObject.children[i].children) {
        let subFolder = createBookmarkObject(folderObject.children[i]);
        folder.children.push(subFolder);
      } else {
        let bookmark = new Bookmark(folderObject.children[i].title, folderObject.children[i].url);
        folder.children.push(bookmark);
      }
    }
    return folder;
  }

  function createFolderElement(folderObject) {
    let bookmarkHtml = "";
    for (let i = 0; i < folderObject.children.length; i++) {
      if (folderObject.children[i] instanceof Folder) {
        bookmarkHtml += createSubFolder(folderObject.children[i]);
      } else {
        bookmarkHtml += createBookmarkElement(folderObject.children[i]) + "\n";
      }
    }
    return "<div class=\"bookmark-set\">\n" +
      "<div class=\"bookmark-title\">" + folderObject.title + "</div>\n" +
      "<div class=\"bookmark-inner-container\">\n" +
      bookmarkHtml +
      "</div>\n" +
      "</div>";
  }

  function createBookmarkElement(bookmarkObject) {
    if (bookmarkObject.title == "Bookmark Manager") {
      return "<div class='bookmarkWrapper'><a id='bookmarkManager' class=\"bookmark\" href=\"javascript:void();\" target=\"_blank\">" + bookmarkObject.title + "</a></div>";
    }
    return "<div class='bookmarkWrapper'><a class=\"bookmark\" href=\"" + bookmarkObject.url + "\" target=\"_blank\">" + bookmarkObject.title + "</a></div>";
  }

  function createSubFolder(bookmarkObject) {
    let begin = "<div class='subfolderWrapper'><div class='bookmarkWrapper'><p>" + bookmarkObject.title + "</p></div>";
    let end = "</div>";
    for (let i = 0; i < bookmarkObject.children.length; i++) {
      if (bookmarkObject.children[i] instanceof Folder) {
        begin += createSubFolder(bookmarkObject.children[i]);
      } else {
        begin += createBookmarkElement(bookmarkObject.children[i]);
      }
    }
    return begin + end;
  }
}

function clock() {
  initClock(document.getElementById("clock"));
  setInterval(() => {
    binaryClock();
  }, 100);
}

function initClock(clock) {
  clock.innerHTML = "" +
    "<div id='hours'><div></div><div></div><div></div><div></div><div></div><div></div<div></div></div>" +
    "<div id='minutes'><div></div><div></div><div></div><div></div><div></div><div></div<div></div></div>" +
    "<div id='seconds'><div></div><div></div><div></div><div></div><div></div><div></div<div></div></div>"
}

function binaryClock() {
  const hours = document.getElementById("hours");
  const minutes = document.getElementById("minutes");
  const seconds = document.getElementById("seconds");

  const timeObject = getTimeObject();

  const drawBinary = (wrapper, number) => {
    function prependZeroes(max, string){
      while(string.length> max){
        string.prepend('0');
      }
      return string;
    }

    function dec2bin(dec) {
      return (dec >>> 0).toString(2);
    }
    let binaryString = prependZeroes(6,dec2bin(number));
    let childrenArray = Array.prototype.slice.call(wrapper.children).reverse();
    let index = 0;
    for (let i = binaryString.length -1; i >= 0; i--) {
      childrenArray[index++].className = binaryString[i] === '1' ? "true" : "false";
    }
  }

  drawBinary(hours, timeObject.hour);
  drawBinary(minutes, timeObject.min);
  drawBinary(seconds, timeObject.sec);
}

// Get current time and format
function getTimeObject() {
  let date = new Date(),
    min = date.getMinutes(),
    sec = date.getSeconds(),
    hour = date.getHours();

  return {hour: hour, min: min, sec: sec};
}


document.addEventListener("keydown", event => {
  if (event.keyCode == 32) {          // Spacebar code to open search
    document.getElementById('search').style.display = 'flex';
    document.getElementById('search-field').focus();
  } else if (event.keyCode == 27) {   // Esc to close search
    document.getElementById('search-field').value = '';
    document.getElementById('search-field').blur();
    document.getElementById('search').style.display = 'none';
  }
});

// Search on enter key event
function search_visibility(e) {
  if (e.keyCode == 27 && (document.getElementById("search").style.display != "" || document.getElementById("search").style.display != "none")) {
    document.getElementById("search").style.display = "";
  }
  if (document.getElementById("search").style.display != "flex") {
    document.getElementById("search").style.display = "flex";
    document.getElementById("search-field").select();
  }
}

function search(e) {
  if (e.keyCode == 13) {
    var val = document.getElementById("search-field").value;
    window.open("https://google.com/search?q=" + val);
  }
}

