class Storage {
    backgroundColor;

    getBackgroundColor() {
        return this.backgroundColor;
    }

    setBackgroundColor(backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}

let storage = new Storage();


document.addEventListener('DOMContentLoaded', function () {
    fetchStorage().then((storage) => {
        addEventListeners(storage);
    })
});

addEventListeners = (storage) => {
    let colorChanger = document.getElementById('colorChange');
    colorChanger.setAttribute("value", storage.getBackgroundColor());

    console.log("should setColor");

    colorChanger.addEventListener("change", (e) =>{
        console.log("eventlistener triggered");
        let color = e.target.value;
        chrome.storage.sync.set({"com.bev.startPage.backgroundColor": color}, function () {
            console.log(color + " background color changed!");
        });
        reloadPage();
    })

};

reloadPage = () => {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
    });
};


loadBackground = (storage) => {
    console.log(storage.getBackgroundColor());
    document.body.style.backgroundColor = storage.getBackgroundColor();
};


fetchStorage = () => {
    return new Promise(((resolve, reject) => {
        chrome.storage.sync.get("com.bev.startPage.backgroundColor", function (color) {
            let rec = color["com.bev.startPage.backgroundColor"];
            console.log("gotColor", rec);
            storage.setBackgroundColor(rec);
            resolve(storage);
        });
    }));
};
